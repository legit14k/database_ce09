﻿using System;
namespace GrimbergJason_CE09
{
    public class DVD
    {
        private string _title;
        private decimal _price;
        private float _rating;

        public string Title { get => _title; set => _title = value; }
        public decimal Price { get => _price; set => _price = value; }
        public float Rating { get => _rating; set => _rating = value; }

        public DVD(string titleParam, decimal priceParam, float ratingParam)
        {
            NewTitle(titleParam);
            NewPrice(priceParam);
            NewRating(ratingParam);
        }

        public string NewTitle(string newTitle)
        {
            Title = newTitle;
            return Title;
        }

        public decimal NewPrice(decimal newPrice)
        {
            Price = newPrice;
            return Price;
        }

        public float NewRating(float newRating)
        {
            Rating = newRating;
            return Rating;
        }
    }
}
