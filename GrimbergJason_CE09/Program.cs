﻿using System;

namespace GrimbergJason_CE09
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            /// Set the name of the connection string
            /// and where the folder is located
            string fileName = "connection.txt";
            string sourcePath = @"c:\VFW";

            // Combine the two to make it have a cleaner look
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            
            // Try and catch the error of it not being there
            try
            {
                string connectionIP = System.IO.File.ReadAllText(sourceFile);

                // Test to see if it grabbed the text from the file
                //Console.WriteLine("Connection IP: {0}", connectionIP);
            }
            catch
            {
                // Throw error if the file doesn't exist
                Console.WriteLine("File path {0} does not exist!", sourceFile);
            }

            // Change title of the program
            Console.Title = "DVD Emporium";
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
